#!/usr/bin/env python
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

import setuptools

setuptools.setup(
    setup_requires=['pbr', 'setuptools'],
    pbr=True
)
